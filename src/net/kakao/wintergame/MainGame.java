package net.kakao.wintergame;

import org.newdawn.slick.*;

import java.util.ArrayList;
import java.util.List;

public class MainGame extends BasicGame {
    private List<Actor> actors;

    public MainGame(String title) {
        super(title);
    }

    public static void main(String[] args) {
        try {
            AppGameContainer container = new AppGameContainer(new MainGame("hm"));
            container.setDisplayMode(800, 600, false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        actors = new ArrayList<>();

        actors.add(new CircleActor(new MoveRight(10, 10, 1), 20));
        actors.add(new CircleActor(new MoveLeft(400, 20, 0.1f), 100));

        actors.add(new RectActor(new MoveRight(100, 200, 0.05f), 50, 60));
    }

    @Override
    public void update(GameContainer gameContainer, int delta) throws SlickException {
        for (Actor actor : actors) {
            actor.update(gameContainer, delta);
        }
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        for (Actor actor : actors) {
            actor.render(gameContainer, graphics);
        }
    }
}
