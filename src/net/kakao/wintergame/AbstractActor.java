package net.kakao.wintergame;

import org.newdawn.slick.GameContainer;

public abstract class AbstractActor implements Actor{
    protected MoveStrategy moveStrategy;

    public AbstractActor(MoveStrategy moveStrategy) {
        this.moveStrategy = moveStrategy;
    }

    public void update(GameContainer gameContainer, int delta) {
        moveStrategy.update(gameContainer, delta);
    }
}
