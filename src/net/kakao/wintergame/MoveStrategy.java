package net.kakao.wintergame;

import org.newdawn.slick.GameContainer;

public interface MoveStrategy {
    float getX();
    float getY();
    void update(GameContainer gameContainer, int delta);
}
