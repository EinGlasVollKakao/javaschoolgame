package net.kakao.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class CircleActor extends AbstractActor {
    private float diameter;

    public CircleActor(MoveStrategy moveStrategy, float diameter) {
        super(moveStrategy);
        this.diameter = diameter;
    }

    public void render(GameContainer gameContainer, Graphics graphics) {
        graphics.drawOval(moveStrategy.getX(), moveStrategy.getY(), this.diameter, this.diameter);
    }
}
