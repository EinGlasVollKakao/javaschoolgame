package net.kakao.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectActor extends AbstractActor {
    private float height, width;

    public RectActor(MoveStrategy moveStrategy, float height, float width) {
        super(moveStrategy);

        this.height = height;
        this.width = width;
    }

    public void render(GameContainer gameContainer, Graphics graphics) {
        graphics.drawRect(moveStrategy.getX(), moveStrategy.getY(), this.width, this.height);
    }
}
